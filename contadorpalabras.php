<?php
echo '<pre>';
$resultado = array();
if (isset($_FILES['nom_fichero'])) {
    $err = $_FILES['nom_fichero']['error'];
    if (!$err) {
        $rutalocal = $_FILES['nom_fichero']['tmp_name'];

        $contenido = file_get_contents($rutalocal);
        $contenido = strtolower($contenido);
        $arraypalabras = preg_split('/[\s!¿?\-\.,;:"\—\(\)\']+/', $contenido, -1, PREG_SPLIT_NO_EMPTY);
        // '/[\s!¿?\-\.,:";\—]+/'

        foreach ($arraypalabras as $palabra) {
            if (!array_key_exists($palabra, $resultado)) {
                $resultado[$palabra] = 1;
            } else {
                $resultado[$palabra]+= 1;
            }
        }
        // var_dump($resultado);
    } else {
        echo "Ha habido un error al subir";
    }
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <style>
            td {
                border: 1px solid black;
                height: 30px;
            }
            .num {
                text-align: right;
            }
        </style>
    </head>
    <body>
        <form method="post" enctype="multipart/form-data">
            <input type="file" name="nom_fichero" /><br />
            <input type="submit" name="enviar" value="Enviar" />
        </form>

        <table>
            <?php
            if (isset($_FILES['nom_fichero'])) {
                echo "<tr><td>PALABRA</td><td>REPETICIONES</td></tr>";
                ksort($resultado);
                foreach ($resultado as $key => $value) {
                    echo "<tr><td>".utf8_encode($key)."</td><td class='num'>$value</td></tr>";
                }
            }
            ?>
        </table>
    </body>
</html>


