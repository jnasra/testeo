<html>
    <head>
        <style>
            .err{color:red} 
            label{display:block}
            .campo{margin-top:9px;display:block}
        </style>
    </head>
    <body>
        <h2>Registro</h2>


        <form method="post">
            <div class="campo">
                <?php inputtext('nombre', "Nombre", $nombre, $errores); ?>
            </div>
            <div class="campo">
                <?php inputtext('edad', "Edad", $edad, $errores); ?>
            </div>
            <div class="campo">
                <?php inputtext('email', "Correo electrónico", $email, $errores); ?>
            </div>
            <div class="campo">
                <?php inputselect('sexo', "Sexo", $sexo, $errores, array('M' => 'Masculino', 'F' => 'Femenino')); ?>
            </div>
            <div class="campo">
                <?php createtextarea('comentarios', "Observaciones", $comentarios); ?>
            </div>
            <div class="campo">
                <?php inputpassword('password', "Contraseña", $password, $errores); ?>
            </div>
            <div class="campo">
                <?php inputcheckbox('condiciones', "Acepto las condiciones de uso", 1, $errores); ?>
            </div>
            <div class="campo">
                <input type="submit" name="accion" value="Enviar" />
            </div>
        </form>
    </body>
</html>

