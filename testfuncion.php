<?php
require 'libfunciones.php';


if (getparam('accion')) {
    $errores = array();
    $nombre = getparam('nombre');
    if (strlen($nombre) < 3)
        $errores['nombre'] = 'Nombre muy corto';
    $edad = getparam('edad');
    if ($edad <= 0 || $edad > 100)
        $errores['edad'] = 'Edad no válida';
    $sexo = getparam('sexo');
    if (!$sexo)
        $errores['sexo'] = 'Dato requerido';
    if (!count($errores)) {
        echo "<h2>DATOS CORRECTOS</h2><a href=?>Empezar</a>";
        die;
    }
} else {
    $nombre = '';
    $edad = '';
    $sexo = '';
    $email ='';
    
}
?>

<h2>Registro</h2>
<style>
    .err{color:red} 
    label{display:block}
    .campo{margin-top:9px;display:block}
</style>

<form method="post">
    <div class="campo">
        <?php inputtext('nombre', "Nombre", $nombre, $errores); ?>
    </div>
    <div class="campo">
        <?php inputtext('edad', "Edad", $edad, $errores); ?>
    </div>
    <div class="campo">
        <?php inputemail('email', "Correo electrónico", $email, $errores); ?>
    </div>
    <div class="campo">
        <?php inputselect('sexo', "Sexo", $sexo, $errores, array('M' => 'Masculino', 'F' => 'Femenino')); ?>
    </div>

    <div class="campo">
        <input type="submit" name="accion" value="Enviar" />
    </div>
</form>


