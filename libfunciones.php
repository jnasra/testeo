<?php

/**
 * Devuelve un dato de POST. 
 * @param type $var  nombre del dato post  ($_POST[$dato))
 * @return string Dato de POST, o "" si no existe
 */
function getparam($var){
    if (isset($_POST[$var]))
        $tmp= trim(htmlspecialchars($_POST[$var], ENT_QUOTES, "UTF-8"));
    else
        $tmp= "";
    
    return $tmp;
}

/** Genera un campo tipo text en un formulario
 * 
 * @param type $name
 * @param type $label
 * @param type $valor
 * @param type $errores
 */
function inputtext($name,$label,$valor,$errores){
     echo "<label>$label</label>";
     echo "<input type='text'  name='$name' value='$valor' />";
     if(isset($errores[$name])) 
         echo '<span class=err>'.$errores[$name].'</span>'; 

}

/** Genera un campo tipo select en un formulario
 * 
 * @param type $name
 * @param type $label
 * @param type $valor
 * @param type $errores
 * @param type $opciones
 */
function inputselect($name,$label,$valor,$errores,$opciones){
     echo "<label>$label</label>";
     echo "<select name='$name' >";
     echo "<option value=''>(Selecciona)</option>";
     foreach($opciones as $opcion=>$texto){
         $selected= ($valor==$opcion)   ? 'selected':'';
         echo "<option value='$opcion' $selected>$texto</option>";
     }
     echo '</select>';
     if(isset($errores[$name])) 
         echo '<span class=err>'.$errores[$name].'</span>'; 

}

/** Genera un campo textarea en un formulario
 * 
 * @param type $name
 * @param type $label
 * @param type $valor
 */
function createtextarea($name,$label,$valor){
     echo "<label>$label</label>";
     echo "<textarea name='$name'>$valor</textarea>";
}

/** Genera un campo tipo password en un formulario
 * 
 * @param type $name
 * @param type $label
 * @param type $valor
 * @param type $errores
 */
function inputpassword($name,$label,$valor,$errores){
     echo "<label>$label</label>";
     echo "<input type='password'  name='$name' value='$valor' />";
     if(isset($errores[$name])) 
         echo '<span class=err>'.$errores[$name].'</span>'; 

}

/** Genera un campo tipo checkbox en un formulario
 * 
 * @param type $name
 * @param type $label
 * @param type $valor
 * @param type $errores
 */
function inputcheckbox($name,$label,$valor,$errores){
     echo "<input type='checkbox'  name='$name' value='$valor'>$label";
     if(isset($errores[$name])) 
         echo '<span class=err>'.$errores[$name].'</span>'; 

}
