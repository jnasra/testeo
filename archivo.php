<?php

echo '<pre>';
if (isset($_FILES['nom_fichero'])) {
    $err = $_FILES['nom_fichero']['error'];
    if (!$err) {
        $rutalocal = $_FILES['nom_fichero']['tmp_name'];
        $tam = $_FILES['nom_fichero']['size'];
        $tipo = $_FILES['nom_fichero']['type'];
        $nombreorigen = $_FILES['nom_fichero']['name'];
        echo "La ruta local es $rutalocal. Tiene un tamaño de $tam. Es de tipo $tipo<br>";
        
        if (substr($nombreorigen, -4) != '.txt') {
            die("No es un texto");
        }
        
        
        $x =  fopen($rutalocal, 'r');
        if (!$x) die("Error al abrir el fichero");
        while ($linea=fgets($x)) {
            echo $linea.'<br>';
        }
        fclose($x);
        
        // la funcion file devuelve el contenido de un fichero separandolo en lineas
        $lineas=file($rutalocal);
        
        foreach ($lineas as $linea) {
            echo $linea.'<br>';
        }
        
        // tercera forma sin lineas, contenido completo de un fichero
        $contenido=  file_get_contents($rutalocal);
        echo $contenido;
        
        
    } else {
        echo "Ha habido un error al subir";
    }
}
var_dump($_POST);
var_dump($_FILES);
?>

<form method="post" enctype="multipart/form-data">
    <input type="file" name="nom_fichero" /><br />
    <input type="submit" name="enviar" value="Enviar" />
</form>