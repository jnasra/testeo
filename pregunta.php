<h2>Practicando las tablas de multiplicar</h2>

<?php
// si venimos del formulario
if (isset($_GET['a']) && isset($_GET['b']) && isset($_GET['r'])) {
    $a = $_GET['a'];
    $b = $_GET['b'];
    $respuesta = $_GET['r'];
    $resp = $a * $b;
    if ($respuesta == $resp) {
        echo ("Correcto");
        $a = rand(1, 9);
        $b = rand(1, 9);
    } else {
        echo "Incorrecto. El valor correcto de " . $a . " x " . $b . " es " . $resp;
    }
} else {
    $a = rand(1, 9);
    $b = rand(1, 9);
}
?>

<form method="get">  
    <h3>Cuanto es <?= $a ?> x <?= $b ?> ?; 
        <input name="a" type="hidden" value="<?= $a ?>">
        <input name="b" type="hidden" value="<?= $b ?>">
        <input type="text" name="r" >
        <input type="submit" value="Comprobar">
    </h3>
</form>

<?php ?>