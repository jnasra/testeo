<?php
$nombre = "";
$edad = "";
$mail = "";
$observaciones = "";
$password = "";
$condiciones = "";
$sexo="";

$errorNombre = "";
$errorEdad = "";
$errorMail = "";
$errorPassword = "";
$errorCondiciones = "";
$errorSexo = "";

$mensajeCorrecto = "";
$correcto = true;

if (isset($_POST['enviar'])) {

    if (isset($_POST['nombre']) && (strlen($_POST['nombre']) > 0)) {
        $nombre = $_POST['nombre'];
    } else {
        $nombre = "";
        $errorNombre = "No has introducido el nombre";
        $correcto = false;
    }

    if (isset($_POST['edad']) && (($_POST['edad']) > 0) && (($_POST['edad']) <= 100)) {
        $edad = $_POST['edad'];
    } else {
        $edad = "";
        $errorEdad = "La edad tiene que estar entre 0 y 100";
        $correcto = false;
    }

    if (isset($_POST['mail']) && strpos($_POST['mail'], "@")) {
        $mail = $_POST['mail'];
    } else {
        $mail = "";
        $errorMail = "No es un email valido";
        $correcto = false;
    }
    
    if (isset($_POST['sexo']) && (($_POST['sexo']==1) || ($_POST['sexo']==2))) {
        $sexo = $_POST['sexo'];
    } else {
        $errorSexo = "  Escoge una opcion";
        $correcto = false;
    }

    if (isset($_POST['password']) && (strlen($_POST['password']) >= 6)) {
        $password = $_POST['password'];
    } else {
        $password = "";
        $errorPassword = "Contraseña incorrecta";
        $correcto = false;
    }

    if (isset($_POST['condiciones']) && ($_POST['condiciones']) == 1) {
        
    } else {
        $condiciones = "";
        $errorCondiciones = "No has aceptado las condiciones de uso";
        $correcto = false;
    }

    $observaciones = $_POST['observaciones'];

    if ($correcto) {
        $mensajeCorrecto = "Usuario guardado correctamente";
    }
}
?>
<html>
    <head>

    </head>
    <body>
        <p>/* 
            Formulario de registro de usuarios
            Desarrolla un script PHP de alta de usuarios en una web, que solicite:
            nombre de usuario
            Edad
            email
            Sexo
            Observaciones
            Contraseña deseada (campo tipo password)
            Un checkbox de "Acepto las condiciones ....."
            Al enviar los datos se validan, y si son correctos se muestra el mensaje "Datos correctos. Usuario dado de alta". Si no lo son,
            se muestra de nuevo el formulario con los valores introducidos para corregirlos y los mensajes de error. Las validaciones a hacer son:
            El nombre de usuario es requerido
            La edad es numérica entre 1 y 100
            La contraseña ha de tener al menos 6 caracteres 
            El email ha de ser correcto (vale con comprobar que lleva una @)
            Se ha marcado la casilla de aceptación de condiciones
            */</p>
        <form method="post">
            <p>Nombre de usuario: <input type="text" name="nombre" value="<?= $nombre; ?>"/><?php echo "$errorNombre" ?></p>
            <p>Edad: <input type="text" name="edad" value="<?= $edad; ?>"/><?php echo "$errorEdad" ?></p>
            <p>Email: <input type="text" name="mail" value="<?= $mail; ?>"/><?php echo "$errorMail" ?></p>
            Sexo: <br>
            <input type="radio" name="sexo" value="1" <?php if ($sexo == 1){echo 'checked="checked"';} ?>> Hombre<?php echo "$errorSexo" ?><br>
            <input type="radio" name="sexo" value="2"<?php if ($sexo == 2){echo 'checked="checked"';} ?>> Mujer<br>
            <p>Observaciones: <textarea name="observaciones"><?= $observaciones; ?></textarea></p>
            <p>Contraseña: <input type="password" name="password" value="<?= $password; ?>"/></p>
            <p><input type="checkbox" name="condiciones" value="1"> Aceptar condiciones de uso<?php echo "$errorCondiciones" ?></p>
            <?php echo "$mensajeCorrecto" ?>
            <p><input type="submit" value="Enviar" name="enviar"/></p>
        </form>
    </body>

</html>





